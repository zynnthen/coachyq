<?php
	    
    if(isset($_POST['submit'])) {
    		
    	$form = $_POST['form'];
		    	
		// EDIT THIS LINE BELOW AS REQUIRED
    	$email_to = "test@localhost.com";
		
    	$from = $_POST['email'];
    	$name = $_POST['name'];
		$phone = $_POST['phone'];
		
		$message = '<html><body>';
		
		if(strcmp($form,"nutrition") == 0) {
			$subject = "Hello Coach YQ - Nutrition";
			$workout = $_POST['workout'];
    		$supplement = $_POST['supplement'];
    		$food = $_POST['food_intake'];
			$reason = $_POST['reason'];
			$serious = $_POST['serious'];
			
			$message .= '<b>Name: </b>' . $name . '<br/>';
			$message .= '<b>Phone: </b>' . $phone . '<br/>';
			$message .= '<b>Email: </b>' . $from . '<br/><br/>';
			$message .= '<b>Workout frequent: </b>' . $workout . '<br/>';
			$message .= '<b>Supplements: </b>' . $supplement . '<br/>';
			$message .= '<b>Healthy food: </b>' . $food . '<br/>';
			$message .= '<b>Reason go for YQ: </b>' . $reason . '<br/>';
			$message .= '<b>Seriousness: </b>' . $serious . '<br/>';
			
		}else if(strcmp($form,"fitness") == 0) {
			$subject = "Hello Coach YQ - Fitness";
			$gym = $_POST['gym'];
			$activities = $_POST['activities'];
			$reason = $_POST['reason'];
			$serious = $_POST['serious'];
			
			$message .= '<b>Name: </b>' . $name . '<br/>';
			$message .= '<b>Phone: </b>' . $phone . '<br/>';
			$message .= '<b>Email: </b>' . $from . '<br/><br/>';
			$message .= '<b>Gym membership: </b>' . $gym . '<br/>';
			$message .= '<b>Favorite activities: </b>' . $activities . '<br/>';
			$message .= '<b>Reason go for YQ: </b>' . $reason . '<br/>';
			$message .= '<b>Seriousness: </b>' . $serious . '<br/>';		
			
		}else if(strcmp($form,"corporate") == 0) {
			$subject = "Hello Coach YQ - Corporate";
			$company = $_POST['company'];
			$designation = $_POST['designation'];
			$help = $_POST['help'];
			$min = $_POST['min'];
			$max = $_POST['max'];		
			
			$message .= '<b>Company: </b>' . $company . '<br/>';
			$message .= '<b>Contact person: </b>' . $name . '<br/>';
			$message .= '<b>Designation: </b>' . $designation . '<br/>';
			$message .= '<b>Phone: </b>' . $phone . '<br/>';
			$message .= '<b>Email: </b>' . $from . '<br/><br/>';
			
			$message .= '<b>Budget: </b>' . $min . ' - ' . $max . '<br/><br/>';
			$message .= '<b>How can we help: </b><br/>' . $help ;
			
		}
		
		$message .= '</body></html>';
		
		$headers  = "From: $from\r\n"; 
    	$headers .= "Content-type: text/html\r\n";

		if (mail($email_to,$subject,$message,$headers)){
			echo ("<SCRIPT LANGUAGE='JavaScript'>
	            window.alert('Thank you! We will contact you shortly.')
	            window.location.href='../index.html';
	            </SCRIPT>");
		}else{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
	            window.alert('Sorry. We are unable to process the request right now. Please try again later.')
	            window.location.href='../index.html';
	            </SCRIPT>");
		}

	}
		
?>
